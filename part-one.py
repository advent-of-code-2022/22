#!/usr/bin/env python3

import os, sys

class Row:
    rows = {}

    def get_starting_tile():
        row_tile_ix = min(Row.rows[0].tiles.keys())
        return Row.rows[0].tiles[row_tile_ix]

    def get_range_for_column(col):
        min_row = sys.maxsize
        max_row = -sys.maxsize
        for row in Row.rows.values():
            if row.row < min_row and row.min_column <= col and row.max_column >= col:
                min_row = row.row
            if row.row > min_row and row.min_column <= col and row.max_column >= col:
                max_row = row.row
        return min_row, max_row

    def get_tiles_for_column(col):
        return [row.tiles[col] for row in Row.rows.values() if col in row.tiles]

    def __init__(self, row, line):
        self.row = row
        self.tiles = {}
        strip_line = line.lstrip(" ")
        left_pad = len(line) - len(strip_line)
        self.min_column = left_pad
        self.max_column = len(line) - 1
        for ix in range(left_pad, len(line)):
            if line[ix] == ".":
                self.tiles[ix] = Tile(self.row, ix)
        for ix in range(left_pad, len(line)-1):
            if ix in self.tiles and (ix+1) in self.tiles:
                self.tiles[ix].right = self.tiles[ix+1]
                self.tiles[ix+1].left = self.tiles[ix]
        min_row_tile_ix = min(self.tiles.keys())
        max_row_tile_ix = max(self.tiles.keys())
        if min_row_tile_ix == left_pad and max_row_tile_ix == (len(line) - 1):
            self.tiles[min_row_tile_ix].left = self.tiles[max_row_tile_ix]
            self.tiles[max_row_tile_ix].right = self.tiles[min_row_tile_ix]
        Row.rows[row] = self

class Column:
    columns = {}

    def __init__(self, col):
        self.tiles = {tile.row: tile for tile in Row.get_tiles_for_column(col)}
        Column.columns[col] = self
        col_top_row, col_bottom_row = Row.get_range_for_column(col)
        for iy in range(col_top_row, col_bottom_row):
            if iy in self.tiles and (iy+1) in self.tiles:
                self.tiles[iy].below = self.tiles[iy+1]
                self.tiles[iy+1].above = self.tiles[iy]
        min_col_tile_ix = min(self.tiles.keys())
        max_col_tile_ix = max(self.tiles.keys())
        if min_col_tile_ix == col_top_row and max_col_tile_ix == col_bottom_row:
            self.tiles[min_col_tile_ix].above = self.tiles[max_col_tile_ix]
            self.tiles[max_col_tile_ix].below = self.tiles[min_col_tile_ix]

class Tile:
    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.above = None
        self.below = None
        self.left = None
        self.right = None

def answer(input_file):
    with open(input_file, "r") as input_data:
        data = input_data.read().split("\n\n")
    
    board = data[0].split("\n")
    moves = data[1]
    moves = moves.replace("R", ":R:").replace("L",":L:").split(":")

    max_row_width = 0
    for iy in range(0, len(board)):
        max_row_width = max(max_row_width, len(board[iy]))
        Row(iy, board[iy])

    for ix in range(0, max_row_width):
        Column(ix)

    selected_tile = Row.get_starting_tile()
    direction = "R"

    directions = {
        "U": ("L","R", lambda: selected_tile.above if selected_tile.above else selected_tile, 3),
        "R": ("U","D", lambda: selected_tile.right if selected_tile.right else selected_tile, 0),
        "D": ("R","L", lambda: selected_tile.below if selected_tile.below else selected_tile, 1),
        "L": ("D","U", lambda: selected_tile.left if selected_tile.left else selected_tile, 2),
    }

    for move in moves:
        if move == "L":
            direction = directions[direction][0]
        elif move == "R":
            direction = directions[direction][1]
        else:
            count = int(move)
            while count > 0:
                count -= 1
                selected_tile = directions[direction][2]()

    answer = ((selected_tile.row+1) * 1000) + ((selected_tile.col+1) * 4) + directions[direction][3]

    print(f"The answer is *** {answer} ***")

input_file = os.path.join(os.path.dirname(__file__), "input")
answer(input_file)
